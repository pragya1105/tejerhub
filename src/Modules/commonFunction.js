var constants = require ('./constant');
var async = require ('async');
var _ = require ('lodash');
var FCM = require('fcm-node');
var config = require ('../Config/development');
var twilio = require('twilio');

exports.generateRandomString = () => {
    let text = "";
    let possible = "123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;  
};


var emailRegex = "^([a-zA-Z0-9_.]+@[a-zA-Z0-9]+[.][.a-zA-Z]+)$";
exports.checkKeyExist = (req, arr) => {
    return new Promise((resolve, reject) => {
        var array = [];
        _.map(arr, (item) => {
            if (req.hasOwnProperty(item)) {
                var value = req[item];
                if (value == '' || value == undefined) {
                    array.push(item + " can not be empty");
                }
                if (item == "password" && value.length < 8) {
                    array.push(constant.responseMessages.PASSWORD_MUST_8_CHARACTER)
                }
 
                resolve(array);
            } else {
                array.push(item + " key is missing");
                resolve(array);
            }
        });
    });
};


exports.sendotp = function(varification_code, mobile_number) {
  

    var twilio = require('twilio');
    var client = new twilio("AC6f236c6c8c0dc188759a840af8959ef0", "66a3763df87026e34c6bf9c9b7698d8f");
    client.messages.create({
            body: "your one time password(OTP) is  " + varification_code + "  valid for 3 minutes do not disclose it",
            to: mobile_number, 
            from: '+18317774782' 
        })
        .then((message) => console.log(message.sid))
        .catch((err) => console.log(err))

}




exports.sendmail = function(help_text, email_id) {
    var nodemailer = require("nodemailer");
    var smtpTransport = require("nodemailer-smtp-transport");
    var config = {
        "SMTP_HOST": "smtp.sendgrid.net",
        "SMTP_PORT": 25,
        "SMTP_USER": "apikey", //default
        "SMTP_PASS": "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"
        //"SMTP_PASS" : config.SMTP_PASS
    }
    var mailer = nodemailer.createTransport(smtpTransport({
        host: config.SMTP_HOST,
        port: config.SMTP_PORT,
        auth: {
            user: config.SMTP_USER,
            pass: config.SMTP_PASS
        }
    }));
    mailer.sendMail({
        from: "email",
        to: email_id,
        cc: "email",
        subject: "Reset Password ",
        template: "text",
        html: `Click here to reset : <a href="${help_text}"> Reset </a>` 
    }, (error, response) => {
        if (error) {
            console.log(error);
        } else {
            console.log(response);
     
        }
        mailer.close();
 
    });
}

/*
-------------------------------------------------
    send mail for key
-------------------------------------------------
*/

exports.sendSecrateKey = function(help_text, email_id) {
    var nodemailer = require("nodemailer");
    var smtpTransport = require("nodemailer-smtp-transport");
    var config = {
        "SMTP_HOST": "smtp.sendgrid.net",
        "SMTP_PORT": 25,
        "SMTP_USER": "apikey",
        "SMTP_PASS": "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"

    }
    var mailer = nodemailer.createTransport(smtpTransport({
        host: config.SMTP_HOST,
        port: config.SMTP_PORT,
        auth: {
            user: config.SMTP_USER,
            pass: config.SMTP_PASS
        }
    }));
    mailer.sendMail({
        from: "Thoughtsunlimited404@gmail.com",
        to: email_id,
        cc: "pragya.kumari@fluper.in",
        subject: "Barber verification code",
        template: "text",
        html: " Your OTP key is :" + help_text
    }, (error, response) => {
        if (error) {
            console.log(error);
            // resolve({ message: "Email not send " });
        } else {
            console.log(response);
            // resolve({ message: "Email send successfully" });
        }
        mailer.close();
        //res.send("Response send successfully");
    });
}




exports.verifyData = (data = {}) => {
    var result = {};
    var count = 0;
    _.map(data, (val, key) => {
        if (val && val.length || _.isInteger(val)) {
            result[key] = val;
        }
    })
    return result;
}

//QR code generator
var qr = require('qr-image');
exports.generateQRcode = function(id) {
    var code = qr.image(id, { type: 'svg' });
    res.type('svg');
    code.pipe(res);
}

// Android push notification

   exports.sendPushNotification = function (serverKey, token, device_type, payload, notify) {
    console.log({payload});
    console.log("send notification Android calling")
         //console.log(serverKey, token, device_type, payload, notify);
            var fcm = new FCM(serverKey);
            var message = {
                to: token,
                collapse_key: 'your_collapse_key',
                //notification: notify,
                data: payload,
             };
             // console.log(message)
             console.log(' a => ');
            fcm.send(message, function(err, response) {
                if (err) {

                    console.log("=======================Android error comming===================")
                    console.log(null, err);
                    console.log('Vishal')
                } else {
                    console.log("=======================Android===================")
                    console.log(null, response)
                }
            }); 
        
    }

//Ios push notification

 exports.sendPushNotificationForIos = function (serverKey, token, device_type, payload, notify) {
    console.log({payload});
     
            var fcm = new FCM(serverKey);
            var message = {
                to: token,
                collapse_key: 'your_collapse_key',
                notification: notify,
                data: payload,
             };

            fcm.send(message, function(err, response) {
                if (err) {
                    console.log("=======================IOS===================")
                    console.log(null, err);
                } else {
                    console.log("=======================IOS===================")
                    console.log(null, response)
                }
            }); 
        
    }



    
exports.twillio = function(varification_code, mobile_number) {
    var client = new twilio(config.accountSid, config.authToken);
    client.messages.create({
            body: "your one time password(OTP) is  " + varification_code + "  valid for 3 minutes do not disclose it",
            to: mobile_number, // Text this number
            from: '(667) 207-2465' // From a valid Twilio number
        })
        .then((message) => console.log(message.sid))
        .catch(err=>console.log(err))
}


